#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc,char* argv[]) {
	int tuberia; // Tuberia FIFO para comunicarse con el logger
	float ra=-1, dec=-1;
	char mensaje[100];

	// Se crea la tuberia para 
	mkfifo("/tmp/Tuberia",0777);
	// Se abre la tuberia 
	tuberia=open("/tmp/Tuberia",O_WRONLY);

	while(ra != 0 && dec != 0) {
		// Se envian las coordenadas al controlador del telescopio 
		// a traves de la tuberia FIFO
		printf("Indica las coordenadas para mover el telescopio\n");
		scanf("%f %f", &ra, &dec);
		printf("Moviendo a RA=%.2f, DEC=%.2f\n", ra, dec);
		sleep(2);
		sprintf(mensaje,"%.2f %.2f", ra, dec);
		write(tuberia, mensaje, strlen(mensaje)+1);
	}
	printf("El telescopio se ha aparcado\n");
	close(tuberia);
	unlink("/tmp/Tuberia");
}
