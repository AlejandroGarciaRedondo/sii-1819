//: C10:Observer.h
// From "Thinking in C++, Volume 2", by Bruce Eckel & Chuck Allison.
// See source code use permissions stated in the file 'License.txt',
// distributed with the code package available at www.MindView.net.
// The Observer interface.
#ifndef OBSERVER_H
#define OBSERVER_H

#include <vector>

class Observable;
class Argument {
public:
	unsigned nParada;
	std::vector<unsigned> conexiones;
};

class Observer {
public:
  // Called by the observed object, whenever
  // the observed object is changed:
  virtual void update(Observable* o, Argument* arg) = 0;
  virtual ~Observer() {}
};
#endif // OBSERVER_H ///:~
