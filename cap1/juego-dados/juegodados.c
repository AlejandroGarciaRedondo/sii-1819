/****************************************************************/
/* Juego de dados												*/
/* CPD															*/
/****************************************************************/
#include <stdio.h>
#include <stdlib.h>


int lanzarDado ( void );


int main ()
{
	int caraDado1, caraDado2;
	int continuarJuego = 1;
	char letra;

	printf("\nJuego de dados, lanzar dos dados y sumar 7 para ganar\n");
	

	while(continuarJuego)
	{
		puts("Pulsar cualquier tecla para lanzar dado 1");
		getchar();
		scanf("%c", &letra);
		caraDado1 = lanzarDado();
		puts("Pulsar cualquier tecla para lanzar dado 2");
		getchar();
		scanf("%c", &letra);
		caraDado2 = lanzarDado();
		if( (caraDado1 + caraDado2) == 7 )
			puts ("Ha ganado");
		else
			puts("Ha perdido");
		puts("Si desea jugar otra vez, pulse C");
		getchar();
		scanf("%c", &letra);
		if ((letra == 'c' )||(letra == 'C'))
			continuarJuego = 1;
		else
			continuarJuego = 0;
	}

	puts("Se termino el juego de dados. Un saludo");
	return 0;
}

int lanzarDado ( void )
{
	int valorCara;
	valorCara=(int)((rand()%6)+1);
	printf("\n%d\n",valorCara);
	return (valorCara);
}

